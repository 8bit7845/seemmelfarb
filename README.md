# Seemmelfarb

![Banner Image](semmelfarbbanner.jpg)

This project is designed to enable easy DIY security for every home and workplace. It consists of two main parts:

* The server, written in Python, which runs on a Raspberry Pi which is connected to a camera module. (located in the RaspiServer folder)
* The application, written in Java, which runs on an Android device. (located in the app folder)

The user is able to watch a live stream, view recorded footage (for as many hours as they choose) and receive alerts when motion is detected.
The communications are handled through a secure SSLSocket so that everything is encrypted, and the user is required to enter a password before he is allowed to view the stream.

## Requirements

To use the project's software you will need to have the following:

**Server side**: 
* A Raspberry Pi computer (tested on 4 and 3B, other versions might work but are untested)
* A camera module which is connected to the Raspberry Pi through the CSI camera port. USB cameras do not work as they have a different method of GPU acceleration.

**Client side**:
* An Android device which runs Android Jellybean 4.2 or newer. Tested on an emulated Nexus 5X, a Samsung Note 8 and other phones.

## Dependencies

To run the server Python code you will need to install the following libraries (pip3 install):
* imutils
* passlib 
* opencv-python==3.4.6.27 (imported as cv2)

Additionally, you will need to install the following packages (apt-get install):
* miniupnpc
* ddclient

To compile the Android Application you will need to install the Android SDK and Java Development Kit (JDK) 8. Compilation was only tested using the Android Studio IDE, manually using the gradle build script should be possible but hasn't been tested.

## Copyright and License

Copyright &copy; 2016-2019 Shawn Baker using the [MIT License](https://opensource.org/licenses/MIT) for the video streaming.
2020 Himmelfarb students for the rest of the interface and the code.

Raspberry image by [Martin Bérubé](http://www.how-to-draw-funny-cartoons.com),
camera image by [Oxygen Team](http://www.oxygen-icons.org).

A special thank you to Roi's friend for the awesome Seemmelfarb banner!

## Sources

[RPi Camera Viewer for Android](http://frozen.ca/rpi-camera-viewer-for-android)

[Streaming Raw H.264 From A Raspberry Pi](http://frozen.ca/streaming-raw-h-264-from-a-raspberry-pi)
