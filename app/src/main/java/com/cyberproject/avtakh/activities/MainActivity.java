// Copyright © 2016-2018 Shawn Baker using the MIT License.
package com.cyberproject.avtakh.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ca.frozen.library.classes.Log;

import com.cyberproject.avtakh.App;
import com.cyberproject.avtakh.BuildConfig;
import com.cyberproject.avtakh.classes.Camera;
import com.cyberproject.avtakh.classes.Utils;
import com.cyberproject.avtakh.R;
import com.cyberproject.avtakh.services.AlertReceiver;

public class MainActivity extends CompatActivityBase
{
	// instance variables
	private int value = 43;
	private String domain = "www.seemmelfarb.live";
	private int streamport = 50000;
	private int settingsport = 50001;
	Button sendpasswordbutton;
	EditText passwordinput;
	String network;
	public final static String CAMERA = "camera";
	Camera streamingcamera;
	Camera settingscamera;
	Runnable passwordCheck;
	Boolean passwordCheckLooping = true;
	ScheduledFuture<?> future;
	//******************************************************************************
	// onCreate
	//******************************************************************************
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		System.out.println("Finished app setup");

		Context context = this;
		domain = getSharedPreferences(getString(R.string.settingfile), MODE_PRIVATE).getString("domain", "www.seemmelfarb.live");
		streamport = getSharedPreferences(getString(R.string.settingfile), MODE_PRIVATE).getInt("streamport", 50000);
		settingsport = getSharedPreferences(getString(R.string.settingfile), MODE_PRIVATE).getInt("settingsport", 50001);

		// set the view
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// initialize the logger
		Utils.initLogFile(getClass().getSimpleName());
		Log.info(getString(R.string.app_name) + " " + BuildConfig.VERSION_NAME);

		// load the settings and cameras
		Utils.loadData();

		// initialize the UI and network name
		network = Utils.getNetworkName();
		sendpasswordbutton = findViewById(R.id.sendpasswordbutton);
		passwordinput = findViewById(R.id.passwordinput);

		App.sendMessage("hello");

		// configuring the polling of the server for the password
		ScheduledExecutorService delayedCheckService = Executors.newScheduledThreadPool(1);
		Runnable passwordCheck = new Runnable() {
			@Override
			public void run() {
				while (passwordCheckLooping) {
					String serverreply = App.receiveMessage();
					if (serverreply.contains("ok")) {
						passwordCheckLooping = false;
						startMenuActivity();
					} else if (serverreply.contains("bad")) {
						runOnUiThread(App.ToasterPrint(context, "Password Incorrect"));
						passwordCheckLooping = false;
					} else if (serverreply.contains("err")) {
						runOnUiThread(() -> App.errorBuilder(context, serverreply).show());
						passwordCheckLooping = false;
					} else {
						runOnUiThread(App.ToasterPrint(context, "Server didn't reply, retrying..."));
						try { Thread.sleep(500);}
						catch (Exception interruption) {System.out.println("Couldn't sleep in passwordCheck!");}
					}
				}
			}
		};

		sendpasswordbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			    // when pressed, send a message to the server with the password and check periodically for a reply
				passwordCheckLooping = true;
				App.sendMessage("pass "+passwordinput.getText().toString());
				ScheduledFuture<?> future = delayedCheckService.schedule(
						passwordCheck, 1000, TimeUnit.MILLISECONDS
				);
			}
		});

		streamingcamera = new Camera(network, domain, streamport);
		settingscamera = new Camera(network, domain, settingsport);

	}

	//******************************************************************************
	// startMenuActivity
	//******************************************************************************
	private void startMenuActivity()
	{
	    //When ready, switch activities
		Log.info("startMenuActivity: " + streamingcamera.name + settingscamera.name);
		Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
		intent.putExtra(MenuActivity.STREAMINGCAMERA, streamingcamera);
		intent.putExtra(MenuActivity.SETTINGSCAMERA, settingscamera);
		startActivity(intent);
	}

	//******************************************************************************
	// onStop
	//******************************************************************************
	@Override
	public void onStop()
	{
		super.onStop();
	}



	//******************************************************************************
	// onResume
	//******************************************************************************
	@Override
	public void onResume()
	{
		Log.setTag(getClass().getSimpleName());
		super.onResume();
		Utils.reloadData();
	}

	//******************************************************************************
	// onSaveInstanceState
	//******************************************************************************
	@Override
	protected void onSaveInstanceState(Bundle state)
	{
		state.putInt("value", value);
		super.onSaveInstanceState(state);
	}

}
