package com.cyberproject.avtakh.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cyberproject.avtakh.App;
import com.cyberproject.avtakh.R;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ChangePasswordActivity extends CompatActivityBase {

    Button changepasswordbutton;
    EditText oldpassword, newpassword, confirmnewpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Context context = this;
        ScheduledExecutorService delayedCheckService = Executors.newScheduledThreadPool(1);

        oldpassword = findViewById(R.id.oldpassword);
        newpassword = findViewById(R.id.newpassword);
        confirmnewpassword = findViewById(R.id.confirmnewpassword);

        changepasswordbutton = findViewById(R.id.changepasswordbutton);
        changepasswordbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String v_oldpassword = oldpassword.getText().toString();
                String v_newpassword = newpassword.getText().toString();
                String v_confirmnewpassword = confirmnewpassword.getText().toString();
                if (v_newpassword.equals(v_confirmnewpassword)) {
                    if (v_newpassword.length() >= 8) {
                        if (!v_newpassword.contains(" ") || !v_oldpassword.contains(" ") || !v_newpassword.contains("~") || !v_oldpassword.contains("~")){
                            App.sendMessage("cp " + v_oldpassword + " " + v_newpassword);
                            delayedCheckService.schedule(
                                    () -> {
                                        String serverreply = App.receiveMessage();
                                        if (serverreply.contains("ok cp")) {
                                            runOnUiThread(App.ToasterPrint(context, "Password has been changed"));
                                        }
                                        else if (serverreply.contains("err")) {
                                            runOnUiThread(() -> App.errorBuilder(context,serverreply).show());
                                        } else {
                                            runOnUiThread(App.ToasterPrint(context, "Failure: The password hasn't been changed"));
                                        }
                                    }, 2000, TimeUnit.MILLISECONDS
                            );
                        }
                        else runOnUiThread(App.ToasterPrint(context, "One of the passwords contains illegal characters!"));
                    }
                    else {
                        runOnUiThread(App.ToasterPrint(context,"The new password has to be longer than 8 characters!"));
                    }
                }
                else {
                    runOnUiThread(App.ToasterPrint(context,"The new password doesn't match the new password confirmation"));
                }
            }
        });
    }
}
