package com.cyberproject.avtakh.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cyberproject.avtakh.App;
import com.cyberproject.avtakh.R;
import com.cyberproject.avtakh.classes.Camera;
import com.cyberproject.avtakh.classes.Utils;

import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ca.frozen.library.classes.Log;

public class MenuActivity extends CompatActivityBase {

    public final static String STREAMINGCAMERA = "streamingcamera";
    public final static String SETTINGSCAMERA = "settingscamera";
    Camera streamingcamera, settingscamera;
    Button viewstreambutton, settingsbutton;
    String network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        network = Utils.getNetworkName();
        Context context = this;

        // get the camera object
        Bundle data = getIntent().getExtras();
        streamingcamera = data.getParcelable(STREAMINGCAMERA);
        settingscamera = data.getParcelable(SETTINGSCAMERA);

        viewstreambutton = findViewById(R.id.viewstreambutton);
        settingsbutton = findViewById(R.id.settingsbutton);

        //setting up delayed network activity
        ScheduledExecutorService delayedCheckService = Executors.newScheduledThreadPool(1);
        ScheduledExecutorService delayedGenericService = Executors.newScheduledThreadPool(1);

        //when view stream button is pressed, check if the stream is OK and connect to it
        viewstreambutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.sendMessage("slv");
                startVideoActivity();
            }
        });

        //when settings button is pressed, send GFL (get file list) command and enter the options activity
        settingsbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.sendMessage("gfl");
                delayedGenericService.schedule(() -> startSettingsActivity(), 2000, TimeUnit.MILLISECONDS);
            }
        });
    }

    //******************************************************************************
    // startCameraActivity
    //******************************************************************************
    private void startCameraActivity(Camera camera)
    {
        Log.info("startCameraActivity: " + camera.name);
        Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
        intent.putExtra(CameraActivity.CAMERA, camera);
        startActivity(intent);
    }

    //******************************************************************************
    // startVideoActivity
    //******************************************************************************
    private void startVideoActivity()
    {
        Log.info("startVideoActivity: " + streamingcamera.name);
        Intent intent = new Intent(getApplicationContext(), VideoActivity.class);
        intent.putExtra(VideoActivity.CAMERA, streamingcamera);
        startActivity(intent);
    }

    //******************************************************************************
    // startSettingsActivity
    //******************************************************************************
    private void startSettingsActivity()
    {
        Log.info("startSettingsActivity: " + settingscamera.name);
        Intent intent = new Intent(getApplicationContext(), OptionsActivity.class);
        intent.putExtra(OptionsActivity.STREAMINGCAMERA, streamingcamera);
        intent.putExtra(OptionsActivity.SETTINGSCAMERA, settingscamera);
        startActivity(intent);
    }

    //******************************************************************************
    // updateCameras
    //******************************************************************************
    public void updateCameras()
    {
        Log.info("updateCameras");
        Collections.sort(Utils.getCameras());
        Utils.saveData();
    }
}
