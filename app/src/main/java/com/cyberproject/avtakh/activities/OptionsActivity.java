// Copyright © 2016-2018 Shawn Baker using the MIT License.
package com.cyberproject.avtakh.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.cyberproject.avtakh.App;
import com.cyberproject.avtakh.R;
import com.cyberproject.avtakh.classes.Camera;
import com.cyberproject.avtakh.classes.Utils;
import com.cyberproject.avtakh.services.AlertReceiver;
import com.cyberproject.avtakh.services.SocketService;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ca.frozen.library.classes.Log;

public class OptionsActivity extends CompatActivityBase
{
	Camera camera;
	BroadcastReceiver receiver;
	SocketService mService;
	public final static String STREAMINGCAMERA = "streamingcamera";
	public final static String SETTINGSCAMERA = "settingscamera";
	Camera streamingcamera, settingscamera;
	EditText domain, streamport, messageport, horizontal_res, vertical_res, recordinghours, fps;
	String v_domain, v_streamport, v_messageport, v_horizontal_res, v_vertical_res, v_recordinghours, v_fps; //values
	Button changepasswordbutton, saveoptionsbutton, replaybutton, tomdbutton, tdmdbutton;
	Spinner dateChoose;
	ArrayAdapter<String> spinnerArrayAdapter;
	String[] responseArray;
	Boolean replayCheckLooping = true;

	SharedPreferences prefs;
	SharedPreferences.Editor editor;

	ScheduledFuture<?> future;
	//******************************************************************************
	// onCreate
	//******************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
		Context context = this;
		prefs = PreferenceManager.getDefaultSharedPreferences(context);

		editor = prefs.edit();

		// configure the activity
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_options);

		// initialize the logger
		Utils.initLogFile(getClass().getSimpleName());

		// get the camera object
		Bundle data = getIntent().getExtras();
		streamingcamera = data.getParcelable(STREAMINGCAMERA);
		settingscamera = data.getParcelable(SETTINGSCAMERA);

		Log.info("camera: " + settingscamera.toString());

		changepasswordbutton = findViewById(R.id.changepassword);
		saveoptionsbutton = findViewById(R.id.saveoptions);
		replaybutton = findViewById(R.id.replay);
		tomdbutton = findViewById(R.id.turnonmd);
		tdmdbutton = findViewById(R.id.turnoffmd);

		//initalize all editable fields
		domain = findViewById(R.id.settings_domain);
		streamport = findViewById(R.id.stream_port);
		messageport = findViewById(R.id.message_port);
		horizontal_res = findViewById(R.id.settings_horizontal_res);
		vertical_res = findViewById(R.id.settings_vertical_res);
		recordinghours = findViewById(R.id.settings_hours);
		fps = findViewById(R.id.settings_FPS);

		//put the current values
		domain.setText(prefs.getString("domain", "www.seemmelfarb.live"));
		streamport.setText(Integer.toString(prefs.getInt("streamport", 50000)));
		messageport.setText(Integer.toString(prefs.getInt("settingsport", 50001)));
		horizontal_res.setText(Integer.toString(prefs.getInt("horizontal_res", 1280)));
		vertical_res.setText(Integer.toString(prefs.getInt("vertical_res", 720)));
		recordinghours.setText(Integer.toString(prefs.getInt("recordinghours", 8)));
		fps.setText(Integer.toString(prefs.getInt("fps", 30)));

		dateChoose = findViewById(R.id.date);

		ScheduledExecutorService delayedStreamCheckService = Executors.newScheduledThreadPool(1);
		ScheduledExecutorService delayedUpdateCheckService = Executors.newScheduledThreadPool(1);
		ScheduledExecutorService delayedCheckService = Executors.newScheduledThreadPool(1);

		//populate the spinner with the dates
		String serverreply = App.receiveMessage();
		System.out.println("Dates message:" + serverreply);
		if (serverreply.contains("ok gfl")){
			String untreatedlist = serverreply.substring(0,serverreply.lastIndexOf("ok gfl"));
			responseArray = untreatedlist.split("\\r?\\n");
			spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, responseArray);
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			runOnUiThread(() -> dateChoose.setAdapter(spinnerArrayAdapter));
		}
		else if (serverreply.contains("err")) {
			runOnUiThread(() -> App.errorBuilder(context,serverreply).show());
		}

		//save settings confirmation
		AlertDialog.Builder WarningBuilder = new AlertDialog.Builder(this);
		WarningBuilder.setMessage("Are you sure you want to change the options? You will have to restart for network changes to take effect.");
		WarningBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						updateValues();
						App.sendMessage("update"+" msg_port "+v_streamport+" stream_port "+v_messageport
								+" height "+v_vertical_res+ " width "+v_horizontal_res+" fps "+v_fps+" hours_to_save "+v_recordinghours);
						//send the settings and await a reply
						delayedUpdateCheckService.schedule(
								() -> {
									String serverreply = App.receiveMessage();
									if (serverreply.contains("ok settings")){
										runOnUiThread(App.ToasterPrint(context,"The options have been updated successfully"));
									}
									else if (serverreply.contains("err")) {
										runOnUiThread(() -> App.errorBuilder(context,serverreply).show());
									}
									else {
										runOnUiThread(App.ToasterPrint(context,"The options have not been updated, try again!"));
									}
								}, 200, TimeUnit.MILLISECONDS
						);
					}
				});

		WarningBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
					}
				});

		//show confirmation for changing settings
		AlertDialog confirmUpdate = WarningBuilder.create();
		saveoptionsbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmUpdate.show();
			}
		});

		//start changepassword activity
		changepasswordbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startChangePasswordActivity();
			}
		});

		//start stream of file
		replaybutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				replayCheckLooping = true;
				App.sendMessage("vfd "+dateChoose.getSelectedItem().toString());
				delayedStreamCheckService.schedule(
						() -> startVideoActivity(), 3000, TimeUnit.MILLISECONDS
				);
			}
		});

		tomdbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Repeating movement check and alert
				AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
				Intent alarmAlertIntent = new Intent(context, AlertReceiver.class);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmAlertIntent, 0);
				alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
						SystemClock.elapsedRealtime(),
						1*60*1000,
						pendingIntent);
				App.showNotification("Motion detection turned on!");

				App.sendMessage("tomd");
				delayedCheckService.schedule(
						() -> {
							String serverreply = App.receiveMessage();
							if (serverreply.contains("ok tomd")){
								runOnUiThread(App.ToasterPrint(context,"Motion detection turrned on"));
							}
							else if (serverreply.contains("err")) {
								runOnUiThread(() -> App.errorBuilder(context,serverreply).show());
							}
							else runOnUiThread(App.ToasterPrint(context,"Server didn't reply"));
						}, 1500, TimeUnit.MILLISECONDS
				);
			}
		});

		tdmdbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				App.sendMessage("tdmd");
				delayedCheckService.schedule(
						() -> {
							String serverreply = App.receiveMessage();
							if (serverreply.contains("ok tdmd")){
								runOnUiThread(App.ToasterPrint(context,"Motion detection turrned off"));
							}
							else if (serverreply.contains("err")) {
								runOnUiThread(() -> App.errorBuilder(context,serverreply).show());
							}
							else runOnUiThread(App.ToasterPrint(context,"Server didn't reply"));
						}, 1500, TimeUnit.MILLISECONDS
				);
			}
		});
	}

	private void updateValues(){
        //updates all values, both as temporary variables and as permanent settings
		v_domain = domain.getText().toString();
		v_streamport = streamport.getText().toString();
		v_messageport = messageport.getText().toString();
		v_horizontal_res = horizontal_res.getText().toString();
		v_vertical_res = vertical_res.getText().toString();
		v_recordinghours = recordinghours.getText().toString();
		v_fps = fps.getText().toString();

		editor.putString("domain", v_domain);
		editor.putInt("streamport", Integer.parseInt(v_streamport));
		editor.putInt("settingsport", Integer.parseInt(v_messageport));
		editor.putInt("horizontal_res", Integer.parseInt(v_horizontal_res));
		editor.putInt("vertical_res", Integer.parseInt(v_vertical_res));
		editor.putInt("recordinghours", Integer.parseInt(v_recordinghours));
		editor.putInt("fps", Integer.parseInt(v_fps));
		editor.commit();
	}

	//******************************************************************************
	// startVideoActivity
	//******************************************************************************
	private void startVideoActivity()
	{
		Log.info("startVideoActivity: " + streamingcamera.name);
		Intent intent = new Intent(getApplicationContext(), VideoActivity.class);
		intent.putExtra(VideoActivity.CAMERA, streamingcamera);
		startActivity(intent);
	}

	//******************************************************************************
	// onOptionsItemSelected
	//******************************************************************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
	{
        if (item.getItemId() == android.R.id.home)
		{
			Log.info("finish");
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
    }

	//******************************************************************************
	// startChangePasswordActivity
	//******************************************************************************
	private void startChangePasswordActivity()
	{
		Log.info("startChangePasswordActivity: " + settingscamera.name);
		Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
		intent.putExtra(MenuActivity.SETTINGSCAMERA, settingscamera);
		startActivity(intent);
	}

    @Override
    protected void onStop() {
		if (receiver != null) {
			unregisterReceiver(receiver);
			receiver = null;
		}
		super.onStop();
	}
}
