// Copyright © 2016 Shawn Baker using the MIT License.
package com.cyberproject.avtakh;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.cyberproject.avtakh.classes.Camera;
import com.cyberproject.avtakh.classes.Utils;
import com.cyberproject.avtakh.services.AlertReceiver;
import com.cyberproject.avtakh.services.SecurityNotificator;
import com.cyberproject.avtakh.services.SocketService;

public class App extends Application
{
    // instance variables
    private static Context context;
    public static SocketService mSocketService;
    public static SecurityNotificator mNotificator;
    public static boolean mNetworkBound;
    public static boolean mNotificationBound;
    private int settingsport = 5001;
    private String network, domain;
    private static String CAMERA = "camera";

    //******************************************************************************
    // onCreate
    //******************************************************************************
    @Override
    public void onCreate()
	{
        super.onCreate();
        context = getApplicationContext();
        network = Utils.getNetworkName();

        System.out.println("Getting preferences...");

        domain = getSharedPreferences(getString(R.string.settingfile), MODE_PRIVATE).getString("domain", "www.seemmelfarb.live");
        settingsport = getSharedPreferences(getString(R.string.settingfile), MODE_PRIVATE).getInt("settingsport", 50001);

        System.out.println("Initializing service...");

        /** Defines callbacks for service binding, passed to bindService() */
        ServiceConnection notificationServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                // We've bound to LocalService, cast the IBinder and get LocalService instance
                SecurityNotificator.LocalBinder binder = (SecurityNotificator.LocalBinder) service;
                mNotificator = binder.getService();
                mNotificationBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) { mNotificationBound = false; }
        };

        // Bind to LocalService
        Intent notificationIntent = new Intent(this, SecurityNotificator.class);
        bindService(notificationIntent, notificationServiceConnection, Context.BIND_AUTO_CREATE);
        System.out.println("Notification Service initialized.");

        /** Defines callbacks for service binding, passed to bindService() */
        ServiceConnection networkServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                // We've bound to LocalService, cast the IBinder and get LocalService instance
                SocketService.LocalBinder binder = (SocketService.LocalBinder) service;
                mSocketService = binder.getService();
                mNetworkBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                mNetworkBound = false;
            }
        };

        // Bind to LocalService
        Intent networkIntent = new Intent(this, SocketService.class);
        networkIntent.putExtra(CAMERA, new Camera(network, domain, settingsport));
        bindService(networkIntent, networkServiceConnection, Context.BIND_AUTO_CREATE);
        System.out.println("Network Service initialized.");
    }


    public static void sendMessage(String message){ //Message sender
        if(mNetworkBound) {
            mSocketService.sendMessage(message);
        }
    }

    public static String receiveMessage(){ //Message receiver, automatically detects end of output (the character ~)
        if(mNetworkBound) {
            String receivedMessage = mSocketService.receiveMessage();
            System.out.println(receivedMessage);
            if (receivedMessage.contains("~")) return receivedMessage.substring(0,receivedMessage.lastIndexOf("~"));
            else return receivedMessage;
        }
        else {
            return "";
        }
    }

    public static void showNotification(String message){ //show a Notification
        if(mNotificationBound) {
            mNotificator.showNotification(message);
        }
    }

    //******************************************************************************
    // Toaster, puts message on screen
    //******************************************************************************
    public static Runnable ToasterPrint(Context context, String message)
    {
        return new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        };
    }

    public static AlertDialog errorBuilder(Context context, String error){
        return new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(error+"\nYou may need to restart to restore the application to working order.")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create();
    }
    //******************************************************************************
    // onCreate
    //******************************************************************************
    public static Context getContext() { return context; }

    //******************************************************************************
    // getStr
    //******************************************************************************
    public static String getStr(int id) { return context.getResources().getString(id); }

	//******************************************************************************
	// getInt
	//******************************************************************************
	public static int getInt(int id)
	{
		return context.getResources().getInteger(id);
	}

	//******************************************************************************
    // getClr
    //******************************************************************************
    public static int getClr(int id)
    {
        return ContextCompat.getColor(context, id);
    }

    //******************************************************************************
    // error
    //******************************************************************************
    public static void error(Context context, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.cancel();
			}
		});
        builder.show();
    }

    //******************************************************************************
    // error
    //******************************************************************************
    public static void error(Context context, int id)
    {
        error(context, getStr(id));
    }

    //******************************************************************************
    // error
    //******************************************************************************
    public static void error(String message)
    {
        error(context, message);
    }

    //******************************************************************************
    // error
    //******************************************************************************
	public static void error(int id)
	{
		error(getStr(id));
	}
}
