package com.cyberproject.avtakh.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.cyberproject.avtakh.App;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AlertReceiver extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Alarming");
        showNewAlert();  //do your task and stop the service when your task is done for battery saving purpose
        stopService(new Intent(this, AlertReceiver.class));

        return super.onStartCommand(intent, flags, startId);
    }

    public void showNewAlert() {
       App.sendMessage("ms");
       ScheduledExecutorService delayedAlertCheckService = Executors.newScheduledThreadPool(1);
       delayedAlertCheckService.schedule(
                () -> {
                    String serverreply = App.receiveMessage();
                    if (serverreply.contains("ago")) App.showNotification(serverreply);
                }, 1000, TimeUnit.MILLISECONDS
        );
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
