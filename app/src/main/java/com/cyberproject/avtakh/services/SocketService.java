package com.cyberproject.avtakh.services;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.cyberproject.avtakh.certificate.trustedSSLContext;
import com.cyberproject.avtakh.classes.Camera;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SocketService extends Service {
    public String SERVERIP; //your computer IP address should be written here
    public int SERVERPORT;
    InputStreamReader in;
    PrintWriter out;
    SSLSocket socket;
    Camera camera;
    Context context;
    Boolean hasSendMessage = false;
    Boolean hasReceiveMessage = false;
    String sendMessage;
    String receiveMessage;
    public final static String CAMERA = "camera";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        Bundle data = intent.getExtras();
        camera = data.getParcelable(CAMERA);
        SERVERIP = camera.address;
        SERVERPORT = camera.port;
        Runnable connect = new connectSocket();
        new Thread(connect).start();
        return myBinder;
    }

    private final IBinder myBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public SocketService getService() {
            System.out.println("I am in the Localbinder of SocketService");
            return SocketService.this;

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public String receiveMessage(){
        if (hasReceiveMessage) {
            hasReceiveMessage = false;
            return receiveMessage;
        }
        return "";
    }
    public void sendMessage(String tosend){
        hasSendMessage = true;
        sendMessage = tosend;
    }

    @Override
    public int onStartCommand(Intent intent,int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        System.out.println("I am in on start");
        Runnable connect = new connectSocket();
        new Thread(connect).start();
        return START_STICKY;
    }

    class connectSocket implements Runnable {
        public void NetworkLoop(){
            while (true) {
                Log.e("TCP Client", "whileLoopActive");
                if (hasSendMessage) {
                    if (out != null && !out.checkError()) {
                        System.out.println("in sendMessage" + sendMessage);
                        out.write(sendMessage);
                        out.flush();
                    }
                    hasSendMessage = false;
                }
                if (!hasReceiveMessage) {
                    if (in != null) {
                        try {
                            char charArray[] = new char[1024];
                            in.read(charArray);
                            receiveMessage = new String(charArray);
                            hasReceiveMessage = true;
                        } catch (Exception e) {
                            receiveMessage = "timed out";
                        }
                    }
                    else {
                        receiveMessage = "in is null!";
                    }
                }
            }
        }
        @Override
        public void run() {
            try {
                Log.e("TCP Client", "C: Connecting...");
                //create a socket to make the connection with the server


                SSLSocketFactory sf = trustedSSLContext.get(context).getSocketFactory();

                SSLSocket socket = (SSLSocket) sf.createSocket(SERVERIP, SERVERPORT);
                socket.setSoTimeout(1000);
                try {
                    //send the message to the server
                    in = new InputStreamReader(socket.getInputStream());
                    out = new PrintWriter(socket.getOutputStream());
                    Log.e("TCP Client", "C: Sent.");
                    Log.e("TCP Client", "C: Done.");

                    Log.e("TCP Client", "C: Entering Main Loop...");

                    NetworkLoop();
                    Log.e("TCP Client", "C: Main Loop Exited");
                }
                catch (Exception e) {
                    Log.e("TCP", "S: Error", e);
                }
            } catch (Exception e) {
                Log.e("TCP", "C: Error", e);
                try {
                    Thread.sleep(1000);
                    run();
                } catch (Exception interruption) {
                    Log.e("TCP", "C: Interrupted while retrying", interruption);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        Log.e("TCP Client", "C: Destroying Socket Service");
        super.onDestroy();
        try {
            socket.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        socket = null;
    }

}