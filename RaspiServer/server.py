import os
import select
import socket
import ssl
from datetime import datetime, date
import multiprocessing
from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2
import atexit
import subprocess
from passlib.hash import sha256_crypt
import errno


def get_files_list():
    '''
    The function return all of the documentation files exist, so the user can choose one of them.
    '''
    s = ""
    # check every file in the folder and append it to string, then the listen_for_request function send it to the user
    # so he could see and choose.
    for file in [f for f in os.listdir(r"/home/pi/Desktop/video_documentation") if f.endswith("h264")]:
        # file for exemple: 230320200745.h264. the next line write it more understandable, like this:
        # '23/03/2020 07:45\n' (\n for next line)
        file_date = str("{day}/{month}/{year} {hour}:{minutes}\n".format(year=file[4:8], month=file[2:4], day=file[:2],
                                                                              hour=file[8:10], minutes=file[10:12]))
        s = s + file_date
    print("the files are: \n", str(s))
    return str(s)



def Checks_if_file_in_documentation_range(file, hours_to_date):
    '''
    Return True if the file need to be delete, False if not.
    '''
    # first the function get both dates to the same form.
    t1 = [int(file[4:8]), int(file[2:4]), int(file[:2]), int(file[8:10]), int(file[10:12])]
    dt = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    hour = int(dt.split(" ")[1][:2])
    minute = int(dt.split(" ")[1][3:5])
    day = int(dt.split(" ")[0][:2])
    month = int(dt.split(" ")[0][3:5])
    year = int(dt.split(" ")[0][6:10])
    t2 = [year, month, day, hour, minute]
    h = [x - y for x, y in zip(t2, t1)] # h will contain list of every diffrent in hour, minuts etc.
    hours_since_date = (h[2] * 24 * 60 + h[3] * 60 + h[4]) / 60 # make all of the changes to hour
    if hours_since_date >= hours_to_date: # for example the file 9 hours and the hours_to_save is 8.
        return True  # the file need to be delete
    else:
        return False


def documentation(queue, update_queue, hours_to_save):
    '''
    :param date_to_save: how many hours the server need to keep documentation of
    :return: nothing. just saving the files.
    '''
    print("documentation is starting now")
    while True:
        try:
            new_hours_to_save = int(update_queue.get())
            hours_to_save == new_hours_to_save
            update_queue.put("")
        except:
            continue
        if hours_to_save == 0:
            time.sleep(5)#for reducing cpu power
            continue
        while True:
            # record video, 15 minute long and save it like this:
            # date(day-2 digit, month- 2 digit, year- 4 digit) hour(hour-2 digit, minutes- 2 digit) .h264.
            # for example "230320200745.h264 this was to save every 10 min, now we need to check all the time if
            # we can delete file because he is not in the date_to_save range
            dt = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            date = "{}{}{}".format(dt[:2], dt[3:5], dt[6:10])  # day, month, year with no space like: 23032020
            hour = dt.split(" ")[1][:2]# 07
            minute = dt.split(" ")[1][3:5]# 45
            try:
                proc = subprocess.Popen(["raspivid -fps 30 -t 900000 -o /home/pi/Desktop/video_documentation/{t}{h}{m}.h264".format(t=date, h=hour,
                m=minute)], stdout=subprocess.PIPE,)
                (out, err) = proc.communicate()
                #activate the command -> save the file acording to the date
            except IOError as e: # if error:
                if e.errno == errno.ENOSPC: # if it an no space left error:
                    queue.put("err: not enofh spaceon your mechin. ")
            # chack if file is out of range, delete if True
            try:
                for file in [f for f in os.listdir(r"/home/pi/Desktop/video_documentation") if
                             f.endswith("h264")]:  # give list of all the video in the folder
                    if Checks_if_file_in_documentation_range(file, hours_to_save):
                        print("file {} removed".format(file))
                        os.remove(r"/home/pi/Desktop/video_documentation/{}".format(file))
            
            except:
                continue


def listen_for_requests(queue, md_queue, IP, PORT):
    while True:
        global client_socket, client_address
        #building the socket
        basic_server_socket = socket.socket()
        basic_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        context = ssl.SSLContext()
        context.load_cert_chain(certfile="cert.pem", keyfile="private.key", password=None)

        server_socket = context.wrap_socket(basic_server_socket, server_side=True, do_handshake_on_connect=True)
        server_socket.bind((IP, PORT))
        server_socket.listen(1)
        print("waiting for connection..")
        (client_socket, client_address) = server_socket.accept()# if somone try to sonnect, like to him.
        print("connected to ", client_address)
        client_socket.settimeout(5) # set timeout so he wont wait infinity untill it recive a msg.

        password_is_correct = False
        while not password_is_correct:
            try:
                password = client_socket.recv(1024).decode().strip().split(" ")[1]
                print("the password is{}".format(password))#get a password feom the user.
            except:
                continue
            try:
                if len(password) > 0:
                    password_is_correct = verify_the_password(password)#check if it correct
                    if not password_is_correct: #if it not correct, send msg to the user and go ack to the while loop's top
                        client_socket.send("bad~".encode())
            except:
                continue
        client_socket.send("ok password~".encode())#send akn msg
        print("password is True :)")
        motion_status = False
        while password_is_correct:
            #first, check if there is alet from the motion_detectio function.
            try:
                alert = md_queue.get(timeout=1)
            except:
                alert = "nope"
            if alert == "movment": # if thare is alert:
                motion_status = True
                start = time.time()#start counting
            #check if thare is err\ok msg that need to be transfer to the user
            try:
                msg_queue = queue.get(timeout=1)
                queue.put("")#if thare is a msg, put empty "" so we wont read it again
            except:
                msg_queue = None
            if msg_queue and ("err" in msg_queue or "ok" in msg_queue): # if thare is ok\err msg, sent that msg.
                print("send this: {}\n to the client".format(msg_queue))
                client_socket.send((msg_queue + "~").encode())

            print("listning for requests...")

            try:
                request = client_socket.recv(1024).decode().strip()#get request from the user
                print("the request is: .", request, ".")
            except:
                continue
            if not request:# if the request is empty, it means that the client disconnected.
                client_socket.close()
                server_socket.close()
                proc = subprocess.Popen(["fuser -k tcp/{}".format(PORT)], stdout=subprocess.PIPE,
                                        shell=True)
                (out, err) = proc.communicate()
                # recreate the socket and reconnect
                print("client disconnected, port and socket closed")
                break
            commend = request.split(" ")[0] #first word in the request
            if commend == "ms":  # motion status. if the client check if thare were alert.
                if motion_status: # if it True
                    end = time.time()# stop counting
                    time_from_movment =int(end - start)
                    client_socket.send(("movment happend {} secounds ago~".format(time_from_movment)).encode())
                    #send alert to the user.
                else:
                    client_socket.send("nothing~".encode())# if thare is no warning, just send "nothing"
            elif "slv" in request:  # stream live video
                queue.put("slv") #transfer the equest to the main function
                client_socket.send(("ok stream~").encode())
            elif "vfd" in request:
                print("its vfd!")
                # stream video from documention
                stream_Video_from_documentation(request, 50000, queue)
                client_socket.send(("ok stream~").encode())
            elif "close stream" in request:
                os.system("killall raspivid ncat")
                print("raspivid and ncat close")
            elif commend == "tomd": 
                md_queue.put("turn on") #transfer the equest to the motion detection function
                client_socket.send("ok tomd".encode())
                print("Aaaaaaa", md_queue.get())
            elif commend == "tdmd":
                md_queue.put("turn off")
                #transfer the equest to the motion detection function
                client_socket.send("ok tdmd".encode())

            elif commend == "update":# update the settings
                # put al the vars and thier value id dict
                msg = request.split(" ")
                d = {}
                d["msg_port"], d["stream_port"], d["hight"], d["width"], d["fps"], d["hours_to_save"] = msg[2],\
                    msg[4], msg[6], msg[8], msg[10], msg[12]
                for i in d:
                    try:
                        int(d[i])
                    except:
                        client_socket.send("err update~".encode())
                        continue
                client_socket.send("ok settings~".encode())
                queue.put(d)
                PORT = msg[2]# if the user cange his port, the next time he connect will be from the updated port
            elif commend == "pass":
                print("back again to the password screen")
                try:
                    if verify_the_password(request.split(" ")[1]):
                        client_socket.send("ok password~".encode())
                        print("password is ok")
                    else:
                        print("password is wrong")
                        client_socket.send("bad~".encode())
                        continue
                except:
                    continue
            elif commend == "quit":
                client_socket.close()
                server_socket.close()
                break
            elif commend == "cp":  # change password, for exemple cp "newpassword"
                    #activate the change_password function with parameters from the user
                    change_password(request.split(" ")[1], request.split(" ")[2], queue)

            elif commend == "gfl":  # get file list. if the user request a list of all the documentation exist.
                try:
                    client_socket.send((get_files_list() + "ok gfl~").encode())
                    queue.put("")
                except Exception as e:
                    print("error with gfl: {}".format(e))
            else:
                print("command:  {} is not valid".format(request))
                client_socket.send(("err: the command: {} is not valid, try again~".format(request)).encode())
                # sent to the uaer err msg
                queue.put("")# so we wont do that again
    

def change_password(previous_password, new_password, queue):
    '''
    :param previous_password: the current password (acording to the user)
    :param new_password: the new password
    :param queue: so he could say to listen_for_request function that the password is incorrect, and the
    listen_for_request can send err to the user.
    :return: nothing. just change the password.
    '''
    try:
        hash_password_from_file = open(r"password.txt", "r").read()
        if '\n' in hash_password_from_file:
            hash_password_from_file = hash_password_from_file[:-1]
        if sha256_crypt.verify(previous_password, hash_password_from_file) == False:
            queue.put("err: the old password is not currect :( ~")
            return False
        os.remove(r"password.txt")
        password_file = open(r"password.txt", "w")
        password_file.write(sha256_crypt.encrypt(new_password))
        password_file.close()
        print("password change.")
    except Exception  as e:
        queue.put("err: {}~".format(e))

def verify_the_password(password):
    """
    check the user password with the password stored in the file, return True\False.
    """
    with open(r"password.txt") as file:
        hash_password_from_file = file.read()
    if '\n' in hash_password_from_file:
        hash_password_from_file = hash_password_from_file[:-1]
    if sha256_crypt.verify(password, hash_password_from_file):
        print("password is True :)")
        return True
    else:
        print("password is not True")
        return False


def stream_video(fps, width, hight, port, queue):
    """ 
    start streaming to the user if the user ask for live stream.
    """
    print("starting to stream in 1 sec:")
    try:
        os.system("raspivid -n -ih -t 0 -rot 0 -w {w} -h {h} -fps {f} -b 1000000 -o - | ncat --ssl --ssl-cert {opdir}/cert.pem --ssl-key {opdir}/private.key -lkv4 {p}".format(
                opdir=os.getcwd(),w=width, h=hight, f=fps, p=port))
  
    except:
        print("An exception occurred/n")
        queue.put("err: idk".format().encode())


def stream_Video_from_documentation(request, port, queue):
    """
    If the user ask for video from the documentation, the message will be in this pattern: "VFD month day hour minute"
    exemple for request: "VFD 03/21/2020 07:45"
    the program will find the video which contains the minutes the user requested and start to stream it to the user.
    """
    print("enter: {}\n...".format(request))
    Date = request.split(" ")[1] + " " + request.split(" ")[2]#23/03/2020 07:45
    request_string_date = Date[0:2] + Date[3:5] + Date[6:10] + Date[11:13] + Date[
                                                     14:17]  # getting it back from 23/03/2020 07:45 to 230320200745.
    file_name = request_string_date + ".h264"
    file_dir = ""
    i=1
    for file in [f for f in os.listdir(r"/home/pi/Desktop/video_documentation") if f.endswith("h264")]:
        current_file_date = (
            "{day}/{month}/{year} {hour}:{minutes}".format(year=file[4:8], month=file[2:4], day=file[:2],
                                                           hour=file[8:10], minutes=file[10:12]))
        if Date == current_file_date:
            file_dir = r"/home/pi/Desktop/video_documentation/{}".format(file_name)
            break
    if file_dir == "":  # if the file not exist
        print("no such file as : {}".format(file_name))
        queue.put("err: file :{} not exist".format(file_name))
        return False
    print("the dir is: {}".format(file_dir))
    os.system("killall ncat raspivid")
    os.system("cat {file} | ncat --ssl --ssl-cert {opdir}/cert.pem --ssl-key {opdir}/private.key -lkv4 {p}".format(opdir=os.getcwd(),file=file_dir, p=port))


def motion_detection(queue):
    """
    The function check all the time if movment accur
    """
    time.sleep(50)
    while True:

        ap = argparse.ArgumentParser()
        ap.add_argument("-v", "--video", help="path to the video file")
        ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
        args = vars(ap.parse_args())

        # if the video argument is None, then we are reading from webcam
        if args.get("video", None) is None:
            vs = VideoStream(src=0).start()
            time.sleep(2.0)

        # initialize the first frame in the video stream
        firstFrame = None

        # loop over the frames of the video
        while True:
            # grab the current frame
            frame = vs.read()
            frame = frame if args.get("video", None) is None else frame[1]

            # if the frame could not be grabbed, then we have reached the end
            # of the video
            if frame is None:
                break

            # resize the frame, convert it to grayscale, and blur it
            frame = imutils.resize(frame, width=500)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)

            # if the first frame is None, initialize it
            if firstFrame is None:
                firstFrame = gray
                continue

            # compute the absolute difference between the current frame and
            # first frame
            frameDelta = cv2.absdiff(firstFrame, gray)
            thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

            # dilate the thresholded image to fill in holes, then find contours
            # on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)

            # loop over the contours
            for c in cnts:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < args["min_area"]:
                    continue
                print("movment!!\n")
                queue.put("movment")
                print("q ok")
                time.sleep(150)#chill time
                queue.put("off")

# cleanup the camera and close any open windows
    vs.stop() if args.get("video", None) is None else vs.release()
    cv2.destroyAllWindows()


def main():
    global motion_detection, documentation
    hours_to_save = 0
    IP = "0.0.0.0"
    msg_port = 50001
    stream_port = 50000
    width = 1280
    height = 780
    fps = 30
    documentation_status, motion_detection_status = False, False  # set them to defult

    #open msg port
    proc = subprocess.Popen(["upnpc -e 'seemmelfarb Connection' -r 50001 TCP"], stdout=subprocess.PIPE,
                            shell=True)
    (out, err) = proc.communicate()
    #open stream port
    proc = subprocess.Popen(["upnpc -e 'seemmelfarb Connection' -r 50000 TCP"], stdout=subprocess.PIPE,
                            shell=True)
    (out, err) = proc.communicate()
    #activate ddclient (dns server)
    proc = subprocess.Popen(["sudo ddclient"], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()

    # creating the queues and give them initial value:
    motion_detection_queue = multiprocessing.Queue()
    listen_for_requests_queue = multiprocessing.Queue()
    update_hours_to_save_queue = multiprocessing.Queue()
    update_motion_detection_queue = multiprocessing.Queue()

    motion_detection_queue.put("")
    listen_for_requests_queue.put("")
    update_hours_to_save_queue.put("")
    update_motion_detection_queue.put("")

    # creat the procces:
    documentation_process = multiprocessing.Process(target=documentation, args=(listen_for_requests_queue,update_hours_to_save_queue, hours_to_save,))
    motion_detection_process = multiprocessing.Process(target=motion_detection, args=(motion_detection_queue,))
    listen_for_requests_process = multiprocessing.Process(target=listen_for_requests, args=(
    listen_for_requests_queue, motion_detection_queue, IP, msg_port,))
    listen_for_requests_process.start()
    motion_detection_process.start()
    while True:
        time.sleep(3)  # for reducing CPU power
        request = listen_for_requests_queue.get()
        print("the request is: ",request)
        print("anybody home?","slv" in request)
        if len(request) > 2:  # if the request is not empty or smaller then 3(the minimum for request)
            if "slv" in request:
                # stream video
                stream_video(fps, width, height, stream_port, listen_for_requests_queue)
                client_socket.send("ok stream".encode())
            elif "vfd" in request:  # video from documentation
                stream_Video_from_documentation(request, stream_port, listen_for_requests_queue)
                client_socket.send("ok stream".encode())
            elif request == "tomd":  # turn on motion detection
                if not motion_detection_status:  # only if the motion_detection_status is False,
                    # if it was already was True skip this.
                    update_motion_detection_queue.put("turn on")
                    motion_detection = True
                    listen_for_requests_queue.put("ok tomd")
                else:
                    print("motion detection was already on")
                listen_for_requests_queue.put("")  # so the request w'ont be executed again
            elif request == "tdmd":  # turn down motion detection
                if motion_detection:  # only if the motion_detection_status is True,
                    # if it was already was False skip this.
                    update_motion_detection_queue.put("turn down")
                    listen_for_requests_queue.put("ok tdmd")
                    print("motion detection turn off")
                else:
                    print("motion detection was already off")
                listen_for_requests_queue.put("ok motion detection")  # so the request w'ont be executed again
            elif type(request) is dict:
                previous_port = stream_port
                previous_msg_port = msg_port
                previous_hours_to_save = hours_to_save
                for key in request:  # set every variable to his new/current value
                    exec(
                        "%s=%d" % (key, int(request[key])))  # execut like python command, for exemple: msg_port = 50001
                if previous_port != stream_port:  # if the user change the port, we need to open a new one
                    proc = subprocess.Popen(
                        ["upnpc -e 'seemmelfarb Connection' -r {} TCP".format(stream_port)],
                        stdout=subprocess.PIPE, shell=True)
                    print("open {} stream port".format(stream_port))
                if previous_msg_port != msg_port:  # if the user change the port, we need to open a new one
                    proc = subprocess.Popen(
                        ["upnpc -e 'seemmelfarb Connection' -r {} TCP".format(msg_port)],
                        stdout=subprocess.PIPE, shell=True)
                    print("open {} msg port".format(msg_port))
                listen_for_requests_queue.put("")  # so the request w'ont be executed again
                if previous_hours_to_save != hours_to_save:
                    update_hours_to_save_queue.put(hours_to_save)
            else:  # if it's not one of the known requests
                print("msg: .{}. \n the msg is not valid, try again:)".format(request))
                listen_for_requests_queue.put("err: the msg: .{}. is not valid, try again:)".format(request))  # so the request w'ont be executed again


if __name__ == '__main__':
    while True:
        try:
            main()
        except:
            continue